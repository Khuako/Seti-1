import com.example.lab3app.data.Employee // Изменили имя модели на Employee
import com.google.gson.annotations.SerializedName

class EmployeeResponse {
    @SerializedName("items") var items: List<Employee>  = emptyList() // Изменили имя модели на Employee
}