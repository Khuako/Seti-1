package com.example.lab3app.repository

import com.example.lab3app.data.Department
import com.example.lab3app.data.Employee
import com.example.lab3app.data.Enterprise
import kotlinx.coroutines.flow.Flow
import java.util.UUID

interface DepartmentDBRepository {
    fun getDepartments(): Flow<List<Department>>
    suspend fun insert(department: Department)
    suspend fun insertListDepartment(departmentList: List<Department>)
    suspend fun deleteDepartment(department: Department)
    suspend fun deleteAllDepartments()

    // Employees
    fun getEmployees(): Flow<List<Employee>>
    fun getDepartmentEmployees(departmentId: UUID): Flow<List<Employee>>
    suspend fun insertEmployee(employee: Employee)
    suspend fun insertListEmployee(employeeList: List<Employee>)
    suspend fun deleteEmployee(employee: Employee)
    suspend fun deleteDepartmentEmployees(departmentId: UUID)
    suspend fun deleteAllEmployees()

    // Enterprises
    fun getEnterprises(): Flow<List<Enterprise>>
    suspend fun insertEnterprise(enterprise: Enterprise)
    suspend fun insertListEnterprise(enterpriseList: List<Enterprise>)
    suspend fun deleteEnterprise(enterprise: Enterprise)
    suspend fun deleteAllEnterprises()
}