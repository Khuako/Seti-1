package com.example.lab3app.api

import com.example.lab3app.data.Enterprise // Изменили имя модели на Enterprise
import com.google.gson.annotations.SerializedName

class EnterprisePost(
    @SerializedName("action") val action: Int,
    @SerializedName("enterprises") val enterprise: Enterprise // Изменили имя модели на Enterprise
)

class EnterpriseResponse {
    @SerializedName("items") var items: List<Enterprise> = emptyList()
}