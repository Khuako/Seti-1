package com.example.lab3app.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val CONNECT_TIMEOUT_INTERVAL_SEC = 10L
private const val READ_TIMEOUT_INTERVAL_SEC = 10L
private const val WRITE_TIMEOUT_INTERVAL_SEC = 10L
private const val BASE_URL = "http://10.0.2.2:5000"
object DepartmentConnection {
    private var retrofit: Retrofit? = null

    private val client = OkHttpClient.Builder()
        .build()
    var gson = GsonBuilder()
        .setDateFormat("yyyy.MM.dd")
        .create()

    fun getClient(): Retrofit{
        if(retrofit == null){
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit!!
    }


}