package com.example.lab3app

import android.app.Application
import android.content.Context
import com.example.lab3app.repository.DepartmentRepository // Добавили импорт

class Application352: Application() {
    override fun onCreate() {
        super.onCreate()
        DepartmentRepository.getInstance().loadData()
    }


    init {
        instance  = this
    }

    companion object{
        private var instance: Application352? = null
        val context
            get() = applicationContext()

        private fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }
}