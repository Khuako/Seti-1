package com.example.lab3app.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.lab3app.MainActivity
import com.example.lab3app.R
import com.example.lab3app.UpdateActivity
import com.example.lab3app.data.Employee
import com.example.lab3app.repository.DepartmentRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

private const val ARG_PARAM1 = "employee_param"
class EmployeeUpdateFragment : Fragment() {
    private lateinit var viewModel: EmployeeListViewModel
    private lateinit var etEmployeeFio: EditText
    private lateinit var etEmployeeYear: EditText
    private lateinit var etEmployeePosition: EditText
    private lateinit var etEmployeePhoneNumber: EditText
    private lateinit var etEmployeeEmail: EditText
    private lateinit var etEmployeeAddress: EditText
    private lateinit var etEmployeeSalary: EditText
    private lateinit var etEmployeeNotes: EditText
    private var employee: Employee? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val param1 = it.getString(ARG_PARAM1)
            if (param1==null)
                employee=Employee()
            else {
                val paramType = object : TypeToken<Employee>() {}.type
                employee = Gson().fromJson<Employee>(param1, paramType)
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.update_employee_fragment, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(EmployeeListViewModel::class.java)

        etEmployeeFio = view.findViewById(R.id.etEmployeeFio)
        etEmployeeYear = view.findViewById(R.id.etYearOfBirth)
        etEmployeePosition = view.findViewById(R.id.etPosition)
        etEmployeePhoneNumber = view.findViewById(R.id.etPhoneNumber)
        etEmployeeEmail = view.findViewById(R.id.etEmail)
        etEmployeeAddress = view.findViewById(R.id.etAddress)
        etEmployeeSalary = view.findViewById(R.id.etSalary)
        etEmployeeNotes = view.findViewById(R.id.etNotes)
        employee?.let {
            etEmployeeFio.setText(it.fio)
            etEmployeeYear.setText(it.yearOfBirth.toString())
            etEmployeePosition.setText(it.position)
            etEmployeePhoneNumber.setText(it.phoneNumber)
            etEmployeeEmail.setText(it.email)
            etEmployeeAddress.setText(it.address)
            etEmployeeSalary.setText(it.salary.toString())
            etEmployeeNotes.setText(it.notes)
        }

        view.findViewById<View>(R.id.btnSaveEmployee).setOnClickListener {
            if(etEmployeeFio.text.toString().isEmpty())
            {
                etEmployeeFio.setError("Заполните поле")
            }else if(etEmployeePosition.text.toString().isEmpty()){
                etEmployeePosition.setError("Заполните поле")
            }else if(etEmployeeYear.text.toString().isEmpty()){
                etEmployeeYear.setError("Заполните поле")
            }else{
                saveEmployee()
            }
        }
    }
    private fun saveEmployee() {
        val fio = etEmployeeFio.text.toString()
        val yearOfBirth = etEmployeeYear.text.toString()
        val position = etEmployeePosition.text.toString()
        val phoneNumber = etEmployeePhoneNumber.text.toString()
        val email = etEmployeeEmail.text.toString()
        val address = etEmployeeAddress.text.toString()
        val salary = etEmployeeSalary.text.toString().toDouble()
        val notes = etEmployeeNotes.text.toString()
        employee?.let {
            it.fio = fio
            it.yearOfBirth = yearOfBirth.toInt()
            it.position = position
            it.phoneNumber = phoneNumber
            it.email = email
            it.address = address
            it.salary = salary
            it.notes = notes
            DepartmentRepository.getInstance().updateEmployee(it)
        }
        (requireContext() as UpdateActivity).setFragmentUpdate(MainActivity.employeeId)
    }
    companion object {
        @JvmStatic
        fun newInstance(employee: Employee) =
            EmployeeUpdateFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, Gson().toJson(employee))
                }
            }
    }
}
