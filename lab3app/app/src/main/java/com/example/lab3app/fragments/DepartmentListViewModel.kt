package com.example.lab3app.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab3app.data.Department
import com.example.lab3app.data.Enterprise
import com.example.lab3app.repository.DepartmentRepository
import java.util.UUID

class DepartmentListViewModel : ViewModel() {
    var departmentList: MutableLiveData<List<Department>> = MutableLiveData()
    var enterpriseList: MutableLiveData<List<Enterprise>> = MutableLiveData()
    private val _filteredDepartmentList = MutableLiveData<LiveData<List<Department>>>()
    private var _department: Department? = null
    val department
        get() = _department

    private var _selectedEnterprise: Enterprise? = null
    val selectedEnterprise
        get() = _selectedEnterprise

    init {
        DepartmentRepository.getInstance().enterpriseList.observeForever { enterprises ->
            enterpriseList.postValue(enterprises)
            _selectedEnterprise = enterprises?.getOrNull(0)

        }

        DepartmentRepository.getInstance().department.observeForever {
            _department = it
        }

        DepartmentRepository.getInstance().departmentList.observeForever { departments ->
            departmentList.postValue(departments)
        }
    }

    fun deleteDepartment() {
        if (department != null) {
            DepartmentRepository.getInstance().deleteDepartment(department!!)
        }
    }

    fun appendDepartment(name: String) {
        val department = Department(name = name, enterpriseId = selectedEnterprise?.id) // Add enterpriseId
        DepartmentRepository.getInstance().newDepartment(department)

    }

    fun updateDepartment(name: String) {
        if (department != null) {
            department!!.name = name
            DepartmentRepository.getInstance().updatedepartment(department!!)
        }
    }
    fun editEnterprise(newName: String) {
        if (selectedEnterprise != null) {
            val updatedEnterprise = selectedEnterprise!!.copy(name = newName)
            DepartmentRepository.getInstance().updateEnterprise(updatedEnterprise)
            DepartmentRepository.getInstance().enterpriseList.observeForever { enterprises ->
                enterpriseList.postValue(enterprises)
                _selectedEnterprise = enterprises?.getOrNull(0)

            }
        } else {
        }
    }

    fun deleteEnterprise() {
        if (selectedEnterprise != null) {
            DepartmentRepository.getInstance().deleteEnterprise(selectedEnterprise!!)
        } else {
        }
    }
    fun setCurrentDepartment(department: Department) {
        DepartmentRepository.getInstance().setCurrentDepartment(department)
    }

    // Method to select a enterprise
    fun selectEnterprise(enterprise: Enterprise) {
        _selectedEnterprise = enterprise
    }

    // Method to add a new enterprise
    fun addNewEnterprise(name: String) {
        val newEnterprise = Enterprise(UUID.randomUUID(), name = name) // You might need to generate a unique ID
        DepartmentRepository.getInstance().addNewEnterprise(newEnterprise)
    }
}
