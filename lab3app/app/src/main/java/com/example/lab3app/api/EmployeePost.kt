package com.example.lab3app.api

import com.example.lab3app.data.Employee // Изменили имя модели на Employee
import com.google.gson.annotations.SerializedName

class EmployeePost(
    @SerializedName("action") val action: Int,
    @SerializedName("employees") val employee: Employee // Изменили имя модели на Employee
)
