package com.example.lab3app.api

import com.example.lab3app.data.Department // Изменили имя модели на Department
import com.google.gson.annotations.SerializedName

class DepartmentResponse {
    @SerializedName("items") lateinit var items: List<Department> // Изменили имя модели на Department
}