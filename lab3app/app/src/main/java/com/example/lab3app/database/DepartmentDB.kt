package com.example.lab3app.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.lab3app.data.Department
import com.example.lab3app.data.Employee
import com.example.lab3app.data.Enterprise

@Database(
    entities = [Department::class, Employee::class, Enterprise::class],
    version = 2,
    exportSchema = false
)

@TypeConverters(DepartmentTypeConverter::class)

abstract class DepartmentDB : RoomDatabase() {
    abstract fun departmentDao(): DepartmentDAO

    companion object {
        @Volatile
        private var INSTANCE: DepartmentDB? = null

        fun getDatabase(context: Context): DepartmentDB {
            return INSTANCE ?: synchronized(this) {
                buildDatabase(context).also { INSTANCE = it }
            }
        }

        //    val MIGRAION_2_3 = object : Migration(2,3){
        //        override fun migrate(db: SupportSQLiteDatabase) {
        //            TODO("Not yet implemented")
        //        }
        //    }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context,
                DepartmentDB::class.java,
                "department_database"
            )
                .fallbackToDestructiveMigration()
                //        .addMigrations(MIGRATION_2_3)
                .build()

    }
}