package com.example.lab3app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.UUID

@Entity(
    indices = [Index("id"), Index("name")]
)

data class Enterprise(
    @PrimaryKey val id: UUID = UUID.randomUUID(),
    @SerializedName("name") @ColumnInfo(name="name") var name: String = "",
)