package com.example.lab3app.api

import com.google.gson.annotations.SerializedName

class PostResult {
    @SerializedName("result") lateinit var resultString: String
}