package com.example.lab3app.repository

import com.example.lab3app.data.Department
import com.example.lab3app.data.Employee
import com.example.lab3app.data.Enterprise
import com.example.lab3app.database.DepartmentDAO
import kotlinx.coroutines.flow.Flow
import java.util.UUID

class DBRepository(val dao: DepartmentDAO) : DepartmentDBRepository {

    override fun getDepartments(): Flow<List<Department>> = dao.getDepartments()

    override suspend fun insert(department: Department) = dao.insertDepartment(department)

    override suspend fun insertListDepartment(departmentList: List<Department>) = dao.insertListDepartment(departmentList)

    override suspend fun deleteDepartment(department: Department) = dao.deleteDepartment(department)

    override suspend fun deleteAllDepartments() = dao.deleteAllDepartments()

    // Employees
    override fun getEmployees(): Flow<List<Employee>> = dao.getEmployees()
    override fun getDepartmentEmployees(departmentId: UUID): Flow<List<Employee>> = dao.getDepartmentEmployees(departmentId)

    override suspend fun insertEmployee(employee: Employee) = dao.insertEmployee(employee)

    override suspend fun insertListEmployee(employeeList: List<Employee>) = dao.insertListEmployee(employeeList)

    override suspend fun deleteEmployee(employee: Employee) = dao.deleteEmployee(employee)

    override suspend fun deleteDepartmentEmployees(departmentID: UUID) = dao.deleteDepartmentEmployees(departmentID)

    override suspend fun deleteAllEmployees() = dao.deleteAllEmployees()

    // Enterprises
    override fun getEnterprises(): Flow<List<Enterprise>> = dao.getEnterprises()

    override suspend fun insertEnterprise(enterprise: Enterprise) = dao.insertEnterprise(enterprise)

    override suspend fun insertListEnterprise(enterpriseList: List<Enterprise>) = dao.insertListEnterprise(enterpriseList)

    override suspend fun deleteEnterprise(enterprise: Enterprise) = dao.deleteEnterprise(enterprise)

    override suspend fun deleteAllEnterprises() = dao.deleteAllEnterprises()
}