package com.example.lab3app.api

import DepartmentPost
import EmployeeResponse
import retrofit2.Call
import retrofit2.http.*

const val GET_DEPARTMENT = 10
const val APPEND_DEPARTMENT = 11
const val UPDATE_DEPARTMENT = 12
const val DELETE_DEPARTMENT = 13

const val GET_EMPLOYEE = 20
const val APPEND_EMPLOYEE = 21
const val UPDATE_EMPLOYEE = 22
const val DELETE_EMPLOYEE = 23

const val GET_ENTERPRISE = 30
const val APPEND_ENTERPRISE = 31
const val UPDATE_ENTERPRISE = 32
const val DELETE_ENTERPRISE = 33

interface ApiInterface {
    // Запросы для отделов
    @GET("?code=$GET_DEPARTMENT")
    fun getDepartments(): Call<DepartmentResponse>

    @Headers("Content-Type: application/json")
    @POST("department")
    fun postDepartment(@Body departmentPost: DepartmentPost): Call<PostResult>

    @Headers("Content-Type: application/json")
    @PUT("department/{department_id}")
    fun updateDepartment(@Path("department_id") departmentId: String, @Body departmentPost: DepartmentPost): Call<PostResult>

    @DELETE("department/{department_id}")
    fun deleteDepartment(@Path("department_id") departmentId: String): Call<PostResult>

    // Запросы для сотрудников
    @GET("employees")
    fun getEmployees(): Call<EmployeeResponse>

    @Headers("Content-Type: application/json")
    @POST("employee")
    fun postEmployee(@Body employeePost: EmployeePost): Call<PostResult>

    @Headers("Content-Type: application/json")
    @PUT("employee/{employee_id}")
    fun updateEmployee(@Path("employee_id") employeeId: String, @Body employeePost: EmployeePost): Call<PostResult>

    @DELETE("employee/{employee_id}")
    fun deleteEmployee(@Path("employee_id") employeeId: String): Call<PostResult>

    // Запросы для предприятий
    @GET("enterprises")
    fun getEnterprises(): Call<EnterpriseResponse>

    @Headers("Content-Type: application/json")
    @POST("enterprise")
    fun postEnterprise(@Body enterprisePost: EnterprisePost): Call<PostResult>

    @Headers("Content-Type: application/json")
    @PUT("enterprise")
    fun updateEnterprise( @Body enterprisePost: EnterprisePost): Call<PostResult>

    @DELETE("enterprise/{enterprise_id}")
    fun deleteEnterprise(@Path("enterprise_id") enterpriseId: String): Call<PostResult>
}