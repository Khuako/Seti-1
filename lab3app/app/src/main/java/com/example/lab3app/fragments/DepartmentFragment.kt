package com.example.lab3app.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lab3app.MainActivity
import com.example.lab3app.R
import com.example.lab3app.UpdateActivity
import com.example.lab3app.data.Department
import com.example.lab3app.databinding.FragmentDepartmentBinding

class DepartmentFragment : Fragment() {


    private lateinit var viewModel: DepartmentListViewModel
    private lateinit var _binding: FragmentDepartmentBinding
    private val binding get() = _binding

    private lateinit var departmentAdapter: DepartmentAdapter
    private var enterpriseId: String? = null

    companion object {
        const val ENTERPRISE_ID_KEY = "enterprise_id"

        fun newInstance(enterpriseId: String): DepartmentFragment {
            val fragment = DepartmentFragment()
            val args = Bundle()
            args.putString(ENTERPRISE_ID_KEY, enterpriseId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDepartmentBinding.inflate(inflater, container, false)
        binding.rvDepartmentList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(DepartmentListViewModel::class.java)
        enterpriseId = arguments?.getString(ENTERPRISE_ID_KEY)

        departmentAdapter = DepartmentAdapter(mutableListOf())
        binding.rvDepartmentList.adapter = departmentAdapter

        viewModel.departmentList.observeForever { departments ->
            val enterpriseCategories = departments.filter { it.enterpriseId.toString() == enterpriseId }
            departmentAdapter.updateItems(enterpriseCategories)
        }
    }

    private inner class DepartmentAdapter(private var items: MutableList<Department>) :
        RecyclerView.Adapter<DepartmentFragment.DepartmentAdapter.ItemHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): DepartmentFragment.DepartmentAdapter.ItemHolder {
            val item = layoutInflater.inflate(R.layout.element_department_list, parent, false)
            return ItemHolder(item!!)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: DepartmentFragment.DepartmentAdapter.ItemHolder, position: Int) {
            holder.bind(items[position])
        }

        private var lastView: View? = null
        private fun updateCurrentView(view: View) {
            lastView?.findViewById<ConstraintLayout>(R.id.clDepartmentElement)
                ?.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
            view.findViewById<ConstraintLayout>(R.id.clDepartmentElement)
                ?.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.my_blue))
            lastView = view
        }

        fun updateItems(newItems: List<Department>) {
            items.clear()
            items.addAll(newItems)
            notifyDataSetChanged()
        }

        inner class ItemHolder(view: View) : RecyclerView.ViewHolder(view) {
            private lateinit var department: Department

            fun bind(department: Department) {
                if (department == viewModel.department) {
                    updateCurrentView(itemView)
                }

                this.department = department
                val tv = itemView.findViewById<TextView>(R.id.tvDepartmentName)
                tv.text = department.name

                val cl = View.OnClickListener {
                    viewModel.setCurrentDepartment(department)
                    updateCurrentView(itemView)
                }
                val c_l = view?.findViewById<ConstraintLayout>(R.id.clDepartmentElement)
                c_l?.setOnClickListener(cl)
                tv.setOnClickListener(cl)
                val lcl = View.OnLongClickListener {
                    viewModel.setCurrentDepartment(department)
                    (requireContext() as UpdateActivity).setFragmentUpdate(MainActivity.employeeId)
                    true
                }
                c_l?.setOnLongClickListener(lcl)
                tv.setOnLongClickListener(lcl)
            }
        }
    }

}