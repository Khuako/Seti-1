package com.example.lab3app.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.lab3app.MainActivity
import com.example.lab3app.R
import com.example.lab3app.UpdateActivity
import com.example.lab3app.data.Department
import com.example.lab3app.data.Enterprise
import com.example.lab3app.databinding.FragmentDepartmentListBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class DepartmentListFragment : Fragment(), MainActivity.Edit {

    companion object {
        private var INSTANCE: DepartmentListFragment? = null

        fun getInstance(): DepartmentListFragment {
            if (INSTANCE == null) INSTANCE = DepartmentListFragment()
            return INSTANCE ?: throw Exception("DepartmentListFragment не создан")
        }
    }

    private lateinit var viewModel: DepartmentListViewModel
    private lateinit var _binding: FragmentDepartmentListBinding
    val binding
        get() = _binding

    private lateinit var departmentAdapter: DepartmentAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDepartmentListBinding.inflate(inflater, container, false)
        binding.rvDepartmentList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(DepartmentListViewModel::class.java)

        viewModel.enterpriseList.observe(viewLifecycleOwner) { enterprises ->
            binding.tabLayout.isVisible = enterprises.isNotEmpty()
            if (enterprises.isNotEmpty()) {
                binding.tabLayout.removeAllTabs()
                enterprises.forEach { enterprise ->
                    binding.tabLayout.addTab(binding.tabLayout.newTab().setText(enterprise.name))
                }

                binding.viewPager.adapter = DepartmentPagerAdapter(this, enterprises)

                TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                    tab.text = enterprises[position].name
                }.attach()

//                binding.tabLayout.selectTab(binding.tabLayout.getTabAt(0))
                viewModel.selectEnterprise(enterprises[0])
                binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                    override fun onTabSelected(tab: TabLayout.Tab) {
                        val selectedEnterprise = enterprises?.getOrNull(tab.position)
                        if (selectedEnterprise != null) {
                            viewModel.selectEnterprise(selectedEnterprise)
                        }
                    }

                    override fun onTabUnselected(tab: TabLayout.Tab) {}
                    override fun onTabReselected(tab: TabLayout.Tab) {}
                })
            } else {
            }
        }

        departmentAdapter = DepartmentAdapter(mutableListOf())

        viewModel.departmentList.observe(viewLifecycleOwner) { categories ->
            departmentAdapter.updateItems(categories)
        }
    }


    override fun editEnterprise() {
        if (viewModel.selectedEnterprise != null) {
            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_enterprise_edit, null)
            val inputName = mDialogView.findViewById<EditText>(R.id.etEnterpriseName)
            inputName.setText(binding.tabLayout.getTabAt(binding.viewPager.currentItem)?.text)

            val dialog = AlertDialog.Builder(requireContext())
                .setTitle("Изменить предприятие")
                .setView(mDialogView)
                .setPositiveButton("Изменить") { dialog, _ ->
                    if (inputName.text.isNotBlank()) {
                        viewModel.editEnterprise(inputName.text.toString())
                        dialog.cancel()
                    } else {
                        inputName.setError("Название предприятия не может быть пустым")
                    }
                }
                .setNegativeButton("Отмена", null)
                .setCancelable(true)
                .create()

            dialog.show()
        } else {
        }
    }

    override fun deleteEnterprise() {
        if (viewModel.selectedEnterprise != null) {
            AlertDialog.Builder(requireContext())
                .setTitle("Удаление предприятия")
                .setMessage("Вы действительно хотите удалить предприятие ${viewModel.selectedEnterprise?.name ?: ""}?")
                .setPositiveButton("Да") { _, _ ->
                    viewModel.deleteEnterprise()
                }
                .setNegativeButton("Нет", null)
                .setCancelable(true)
                .create()
                .show()
        } else {
        }
    }

    override fun appendDepartment() {
        newDepartment()
    }

    override fun editDepartment() {
        updateDepartment()
    }

    // ... (other methods) ...
    private inner class DepartmentPagerAdapter(fragment: Fragment, private val enterprises: List<Enterprise>) :
        FragmentStateAdapter(fragment) {

        override fun createFragment(position: Int): Fragment {
            val enterprise = enterprises[position]
            return DepartmentFragment.newInstance(enterprise.id.toString())
        }

        override fun getItemCount(): Int = enterprises.size
    }

    private inner class DepartmentAdapter(private var items: MutableList<Department>) :
        RecyclerView.Adapter<DepartmentAdapter.ItemHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): DepartmentAdapter.ItemHolder {
            val item = layoutInflater.inflate(R.layout.element_department_list, parent, false)
            return ItemHolder(item!!)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: DepartmentAdapter.ItemHolder, position: Int) {
            holder.bind(items[position])
        }

        private var lastView: View? = null
        private fun updateCurrentView(view: View) {
            lastView?.findViewById<ConstraintLayout>(R.id.clDepartmentElement)
                ?.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
            view.findViewById<ConstraintLayout>(R.id.clDepartmentElement)
                ?.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.my_blue))
            lastView = view
        }

        fun updateItems(newItems: List<Department>) {
            items.clear()
            items.addAll(newItems)
            notifyDataSetChanged()
        }

        inner class ItemHolder(view: View) : RecyclerView.ViewHolder(view) {
            private lateinit var department: Department

            fun bind(department: Department) {
                if (department == viewModel.department) {
                    updateCurrentView(itemView)
                }

                this.department = department
                val tv = itemView.findViewById<TextView>(R.id.tvDepartmentName)
                tv.text = department.name

                val cl = View.OnClickListener {
                    viewModel.setCurrentDepartment(department)
                    updateCurrentView(itemView)
                }
                val c_l = view?.findViewById<ConstraintLayout>(R.id.clDepartmentElement)
                c_l?.setOnClickListener(cl)
                tv.setOnClickListener(cl)
                val lcl = View.OnLongClickListener {
                    viewModel.setCurrentDepartment(department)
                    (requireContext() as UpdateActivity).setFragmentUpdate(MainActivity.employeeId)
                    true
                }
                c_l?.setOnLongClickListener(lcl)
                tv.setOnLongClickListener(lcl)
            }
        }
    }

    override fun appendEnterprise() {
        addEnterprise()
    }

    override fun append() {

    }


    override fun update() {
    }

    override fun delete() {
    }

    private fun newDepartment() {
        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_department_edit, null)
        val inputName = mDialogView.findViewById<EditText>(R.id.etDepartmentName)

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle("Информация об категории")
            .setView(mDialogView)
            .setPositiveButton("Добавить") { dialog, _ ->
                if (inputName.text.isNotBlank()) {
                    viewModel.appendDepartment(inputName.text.toString())
                    dialog.cancel()
                } else {
                    inputName.setError("Название категории не может быть пустым")
                }
            }
            .setNegativeButton("Отмена") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(true)
            .create()

        dialog.show()
    }
    private fun addEnterprise() {
        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_enterprise_edit, null)
        val inputName = mDialogView.findViewById<EditText>(R.id.etEnterpriseName)

        val dialog = AlertDialog.Builder(requireContext())
            .setTitle("Добавить предприятие")
            .setView(mDialogView)
            .setPositiveButton("Добавить") { dialog, _ ->
                if (inputName.text.isNotBlank()) {
                    viewModel.addNewEnterprise(inputName.text.toString())
                    dialog.cancel() // Close the dialog
                } else {
                    inputName.setError("Название предприятия не может быть пустым")
                }
            }
            .setNegativeButton("Отмена") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(true)
            .create()

        dialog.show()
    }
    private fun updateDepartment() {
        val mDialogView =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_department_edit, null)
        val inputName = mDialogView.findViewById<EditText>(R.id.etDepartmentName)
        inputName.setText(viewModel.department?.name)

        AlertDialog.Builder(requireContext())
            .setTitle("Изменить информацию о категории")
            .setView(mDialogView)
            .setPositiveButton("Изменить") { _, _ ->
                if (inputName.text.isNotBlank()) {
                    viewModel.updateDepartment(inputName.text.toString())
                } else {
                    inputName.setError("Название категории не может быть пустым")
                }
            }
            .setNegativeButton("Отмена", null)
            .setCancelable(true)
            .create()
            .show()
    }

    override fun deleteDepartment() {
        AlertDialog.Builder(requireContext())
            .setTitle("Удаление!")
            .setMessage("Вы действительно хотите удалить категорию ${viewModel.department?.name ?: ""}?")
            .setPositiveButton("Да") { _, _ ->
                viewModel.deleteDepartment()
            }
            .setNegativeButton("Нет", null)
            .setCancelable(true)
            .create()
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as UpdateActivity).setTitle("Список Предприятий")
    }
}
