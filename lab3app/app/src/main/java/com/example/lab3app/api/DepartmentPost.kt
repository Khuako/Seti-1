import com.example.lab3app.data.Department // Изменили имя модели на Department
import com.google.gson.annotations.SerializedName

class DepartmentPost(
    @SerializedName("action") val action: Int,
    @SerializedName("departments") val department: Department // Изменили имя модели на Department
)