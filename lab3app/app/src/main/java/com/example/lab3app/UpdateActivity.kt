package com.example.lab3app

import com.example.lab3app.data.Employee
import java.util.UUID

interface UpdateActivity {
    fun setTitle(_title: String)
    fun setFragmentUpdate(fragmentId: Int, employee: Employee? = null, enterpriseId: UUID? = null)
}