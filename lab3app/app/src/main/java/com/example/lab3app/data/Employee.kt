package com.example.lab3app.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.UUID

@Entity(
    tableName = "employees",
    indices = [Index("id"), Index("department_id")],
)
data class Employee(
    @PrimaryKey val id: UUID = UUID.randomUUID(),
    @SerializedName("fio") var fio: String = "",
    @SerializedName("yearOfBirth") var yearOfBirth: Int = 0,
    @SerializedName("position") var position: String = "",
    @SerializedName("department_id") @ColumnInfo(name = "department_id") var departmentId: UUID? = null,
    @SerializedName("phoneNumber") var phoneNumber: String = "",
    @SerializedName("email") var email: String = "",
    @SerializedName("address") var address: String = "",
    @SerializedName("salary") var salary: Double = 0.0,
    @SerializedName("notes") var notes: String = ""
)