package com.example.lab3app.fragments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lab3app.data.Employee
import com.example.lab3app.repository.DepartmentRepository

class EmployeeListViewModel : ViewModel() {
    var employeeList: MutableLiveData<List<Employee>> = MutableLiveData()
    private var _employee: Employee? = null

    val employee
        get() = _employee


    val department
        get() = DepartmentRepository.getInstance().department.value


    init{
        DepartmentRepository.getInstance().employeeList.observeForever{
            if(it.isNotEmpty()){
                employeeList.postValue(
                    it.filter { it.departmentId == DepartmentRepository.getInstance().department.value?.id }
                        .sortedBy { it.id }
                )
            }
        }
    }

    fun deleteEmployee(){
        if(employee != null){
            DepartmentRepository.getInstance().deleteEmployee(employee!!)
        }
    }

    fun appendEmployee(fio: String, yearOfBirth: Int, position: String){
        val employee = Employee()
        employee.departmentId = DepartmentRepository.getInstance().department.value?.id
        employee.fio = fio
        employee.yearOfBirth = yearOfBirth
        employee.position = position
        DepartmentRepository.getInstance().newEmployee(employee)
    }
    fun updateEmployee(
        fio: String,
        yearOfBirth: Int,
        position: String,
        phoneNumber: String,
        email: String,
        address: String,
        salary: Double,
        notes: String
    ) {
        print("fasfad")
        _employee?.let {
            it.fio = fio
            it.yearOfBirth = yearOfBirth
            it.position = position
            it.phoneNumber = phoneNumber
            it.email = email
            it.address = address
            it.salary = salary
            it.notes = notes
            DepartmentRepository.getInstance().updateEmployee(it)
        }
    }

    fun setCurrentEmployee(employee: Employee){
        _employee = employee;
        DepartmentRepository.getInstance().setCurrentEmployee(employee)
    }

}