package com.example.lab3app.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lab3app.MainActivity
import com.example.lab3app.R
import com.example.lab3app.UpdateActivity
import com.example.lab3app.data.Employee
import com.example.lab3app.databinding.FragmentEmployeeListBinding

class EmployeeListFragment : Fragment(), MainActivity.Edit {


    companion object {
        private var INSTANCE: EmployeeListFragment? = null

        fun getInstance(): EmployeeListFragment {
            INSTANCE = EmployeeListFragment()
            return INSTANCE ?: throw Exception("EmployeeListFragment не создан")
        }
    }

    private lateinit var viewModel: EmployeeListViewModel
    private lateinit var _binding: FragmentEmployeeListBinding
    val binding
        get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEmployeeListBinding.inflate(inflater, container, false)
        binding.rvEmployeeList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.employeeList.observe(viewLifecycleOwner) {
            binding.rvEmployeeList.adapter = EmployeeAdapter(it!!)
        }
        binding.fabNewEmployee.setOnClickListener {
            append()
        }
    }



    private inner class EmployeeAdapter(private val items: List<Employee>) :

        RecyclerView.Adapter<EmployeeAdapter.ItemHolder>() {
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): EmployeeAdapter.ItemHolder {
            val item = layoutInflater.inflate(R.layout.element_employee_list, parent, false)
            return ItemHolder(item!!)
        }

        override fun getItemCount(): Int = items.size
        override fun onBindViewHolder(holder: EmployeeAdapter.ItemHolder, position: Int) {
            holder.bind(viewModel.employeeList.value!![position])
        }

        private fun sortByYear() {
            if (viewModel.employeeList.value != null) {
                if (viewModel.employeeList.value!!.first() == viewModel.employeeList.value?.sortedBy { it.yearOfBirth }!!
                        .first()
                ) {
                    viewModel.employeeList.value =
                        viewModel.employeeList.value?.sortedBy { it.yearOfBirth }!!.reversed()
                } else {
                    viewModel.employeeList.value = viewModel.employeeList.value?.sortedBy { it.yearOfBirth }
                }
            }
        }

        private fun sortByName() {
            if (viewModel.employeeList.value != null) {
                if (viewModel.employeeList.value!!.first() == viewModel.employeeList.value?.sortedBy { it.fio }!!
                        .first()
                ) {
                    viewModel.employeeList.value =
                        viewModel.employeeList.value?.sortedBy { it.fio }!!.reversed()
                } else {
                    viewModel.employeeList.value = viewModel.employeeList.value?.sortedBy { it.fio }
                }
            }
        }

        private var lastView: View? = null
        private fun updateCurrentView(view: View) {
            lastView?.findViewById<LinearLayout>(R.id.clEmployeeElement)
                ?.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white))
            view.findViewById<LinearLayout>(R.id.clEmployeeElement)
                ?.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        androidx.appcompat.R.color.error_color_material_light
                    )
                )
            lastView = view

        }

        private inner class ItemHolder(view: View) : RecyclerView.ViewHolder(view) {
            private lateinit var employee: Employee
            val cl = View.OnClickListener {
                viewModel.setCurrentEmployee(employee)
                val mDialogView =
                    LayoutInflater.from(requireContext())
                        .inflate(R.layout.dialog_employee_details, null)
                mDialogView.findViewById<TextView>(R.id.tvPosition).text =
                    employee.position
                AlertDialog.Builder(requireContext())
                    .setTitle("Информация о сотруднике")
                    .setView(mDialogView)
                    .setCancelable(true)
                    .setNegativeButton("Удалить") { _, _ ->
                        deleteEmployee()
                    }
                    .setPositiveButton("Изменить") { _, _ ->

                        (requireActivity() as UpdateActivity).setFragmentUpdate(2, employee)
                        viewModel.setCurrentEmployee(employee)
                        onDestroy()
                        onDestroyView()
                    }
                    .create()
                    .show()
                updateCurrentView(itemView)
            }
            fun bind(employee: Employee) {


                this.employee = employee
                Log.d(tag, employee.toString())
                val nameText = itemView.findViewById<TextView>(R.id.tvEmployeeFio)
                nameText.setOnLongClickListener {
                    sortByName()
                    true
                }
                nameText.text = employee.fio
                val yearText = itemView.findViewById<TextView>(R.id.tvEmployeeYear)
                yearText.setOnLongClickListener {
                    sortByYear()
                    true
                }
                yearText.text = employee.yearOfBirth.toString() + " руб/мес"



                nameText.setOnClickListener(cl)
                yearText.setOnClickListener(cl)
            }
        }
    }

    override fun append() {
        newEmployee()
    }

    override fun delete() {
        deleteEmployee()
    }

    override fun appendEnterprise() {
        TODO("Not yet implemented")
    }

    override fun editEnterprise() {
        TODO("Not yet implemented")
    }

    override fun deleteEnterprise() {
        TODO("Not yet implemented")
    }

    override fun appendDepartment() {
        TODO("Not yet implemented")
    }

    override fun editDepartment() {
        TODO("Not yet implemented")
    }

    override fun deleteDepartment() {
        TODO("Not yet implemented")
    }



    override fun update() {
    }

    private fun newEmployee() {
        val mDialogView =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_employee, null)
        val etEmployeeName: EditText = mDialogView.findViewById(R.id.etEmployeeFio)
        val etEmployeeYear: EditText = mDialogView.findViewById(R.id.etYearOfBirth)
        val etEmployeePosition: EditText = mDialogView.findViewById(R.id.etPosition)

        AlertDialog.Builder(requireContext())
            .setTitle("Нанять сотрудника")
            .setView(mDialogView)
            .setPositiveButton("Добавить") { _, _ ->
                viewModel.appendEmployee(
                    etEmployeeName.text.toString(),
                    etEmployeeYear.text.toString().toInt(),
                    etEmployeePosition.text.toString(),
                );
            }
            .setNegativeButton("Отмена", null)
            .setCancelable(true)
            .create()
            .show()
    }

    private fun deleteEmployee() {
        AlertDialog.Builder(requireContext())
            .setTitle("Удаление!")
            .setMessage("Вы действительно хотите уволить сотрудника ${viewModel.employee?.fio ?: ""}?")
            .setPositiveButton("Да") { _, _ ->
                viewModel.deleteEmployee()
            }
            .setNegativeButton("Нет", null)
            .setCancelable(true)
            .create()
            .show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProvider(this).get(EmployeeListViewModel::class.java)
        (context as UpdateActivity).setTitle("Сотрудники ${viewModel.department?.name ?: ""}")
    }

}