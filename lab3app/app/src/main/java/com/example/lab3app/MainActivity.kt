package com.example.lab3app

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.lab3app.data.Employee
import com.example.lab3app.fragments.DepartmentListFragment
import com.example.lab3app.fragments.EmployeeListFragment
import com.example.lab3app.fragments.EmployeeUpdateFragment
import com.example.lab3app.repository.DepartmentRepository
import java.util.UUID

class MainActivity : AppCompatActivity(), UpdateActivity {
    interface Edit {
        fun append()
        fun update()
        fun delete()
        fun appendEnterprise()
        fun editEnterprise()
        fun deleteEnterprise()
        fun appendDepartment()
        fun editDepartment()
        fun deleteDepartment()
    }

    companion object {
        const val departmentId = 0
        const val employeeId = 1
        const val employeeInfoId = 2
    }

    private var _miNewEnterprise: MenuItem? = null
    private var _miUpdateEnterprise: MenuItem? = null
    private var _miDeleteEnterprise: MenuItem? = null
    private var _miNewDepartment: MenuItem? = null
    private var _miUpdateDepartment: MenuItem? = null
    private var _miDeleteDepartment: MenuItem? = null

    private var _selectedEnterpriseId: UUID? = null
    val selectedEnterpriseId: UUID?
        get() = _selectedEnterpriseId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onBackPressedDispatcher.addCallback(this) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                supportFragmentManager.popBackStack()
                when (currentFragmentID) {

                    departmentId -> {
                        currentFragmentID = departmentId
                        setTitle("Список предприятий")
                    }

                    employeeId -> {
                        currentFragmentID = departmentId
                        setTitle("Список предприятий")
                    }

                    employeeInfoId -> {
                        currentFragmentID = employeeId
                        setTitle("Сотрудники ${DepartmentRepository.getInstance().department.value?.name}")
                    }

                    else -> {}
                }
                updateMenuView()
            } else {
                finish()
            }
        }
        setFragmentUpdate(fragmentId = currentFragmentID, employee = null, enterpriseId = null)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        _miNewEnterprise = menu?.findItem(R.id.miNewEnterprise)
        _miUpdateEnterprise = menu?.findItem(R.id.miEditEnterprise)
        _miDeleteEnterprise = menu?.findItem(R.id.miDeleteEnterprise)
        _miNewDepartment = menu?.findItem(R.id.miNewDepartment)
        _miUpdateDepartment = menu?.findItem(R.id.miUpdateDepartment)
        _miDeleteDepartment = menu?.findItem(R.id.miDeleteDepartment)
        updateMenuView()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.miNewDepartment -> {
                val fedit: Edit = DepartmentListFragment.getInstance()
                fedit.appendDepartment()
                true
            }

            R.id.miUpdateDepartment -> {
                val fedit: Edit = DepartmentListFragment.getInstance()
                fedit.editDepartment()
                true
            }

            R.id.miDeleteDepartment -> {
                val fedit: Edit = DepartmentListFragment.getInstance()
                fedit.deleteDepartment()
                true
            }

            R.id.miNewEnterprise -> {
                val fedit: Edit = DepartmentListFragment.getInstance()
                fedit.appendEnterprise()
                true
            }

            R.id.miEditEnterprise -> {
                val fedit: Edit = DepartmentListFragment.getInstance()
                fedit.editEnterprise()
                true
            }

            R.id.miDeleteEnterprise -> {
                val fedit: Edit = DepartmentListFragment.getInstance()
                fedit.deleteEnterprise()
                true
            }


            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setTitle(_title: String) {
        title = _title
    }

    private var currentFragmentID = 0

    override fun setFragmentUpdate(fragmentId: Int, employee: Employee?, enterpriseId: UUID?) {
        currentFragmentID = fragmentId
        _selectedEnterpriseId = enterpriseId

        when (fragmentId) {

            departmentId -> {
                setTitle("Список отделов")
                setFragment(DepartmentListFragment.getInstance())
            }

            employeeId -> {
                setTitle("Список сотрудников")
                setFragment(EmployeeListFragment.getInstance())
            }

            employeeInfoId -> {
                setTitle("Информация о сотруднике") // Установите заголовок
                setFragment(EmployeeUpdateFragment.newInstance(employee ?: Employee()))
            }
        }
    }

    private fun setFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fcwMain, fragment)
            .addToBackStack(null)
            .commit()
        updateMenuView()
    }

    private fun updateMenuView() {
        _miNewEnterprise?.isVisible = currentFragmentID == departmentId
        _miUpdateEnterprise?.isVisible = currentFragmentID == departmentId
        _miDeleteEnterprise?.isVisible = currentFragmentID == departmentId
        _miNewDepartment?.isVisible = currentFragmentID == departmentId
        _miUpdateDepartment?.isVisible = currentFragmentID == departmentId
        _miDeleteDepartment?.isVisible = currentFragmentID == departmentId
    }
}