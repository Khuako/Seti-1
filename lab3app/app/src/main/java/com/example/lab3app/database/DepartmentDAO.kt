package com.example.lab3app.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.lab3app.data.Department
import com.example.lab3app.data.Employee
import com.example.lab3app.data.Enterprise
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Dao
interface DepartmentDAO {
    @Query("select * from Department order by name")
    fun getDepartments(): Flow<List<Department>>

    @Insert(entity = Department::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDepartment(department: Department)

    @Insert(entity = Department::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertListDepartment(departmentList: List<Department>)

    @Delete(entity = Department::class)
    suspend fun deleteDepartment(department: Department)

    @Query("delete from Department")
    suspend fun deleteAllDepartments()

    // Employees
    @Query("select * from employees order by fio")
    fun getEmployees(): Flow<List<Employee>>

    @Query("select * from employees where department_id=:departmentId")
    fun getDepartmentEmployees(departmentId: UUID): Flow<List<Employee>>

    @Insert(entity = Employee::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmployee(employee: Employee)

    @Insert(entity = Employee::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertListEmployee(employeeList: List<Employee>)

    @Delete(entity = Employee::class)
    suspend fun deleteEmployee(employee: Employee)

    @Query("delete from employees where department_id=:departmentId")
    suspend fun deleteDepartmentEmployees(departmentId: UUID)

    @Query("delete from employees")
    suspend fun deleteAllEmployees()

    // Enterprises
    @Query("select * from Enterprise order by name")
    fun getEnterprises(): Flow<List<Enterprise>>

    @Insert(entity = Enterprise::class, onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEnterprise(enterprise: Enterprise)

    @Insert(entity = Enterprise::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertListEnterprise(enterpriseList: List<Enterprise>)

    @Delete(entity = Enterprise::class)
    suspend fun deleteEnterprise(enterprise: Enterprise)

    @Query("delete from Enterprise")
    suspend fun deleteAllEnterprises()
}