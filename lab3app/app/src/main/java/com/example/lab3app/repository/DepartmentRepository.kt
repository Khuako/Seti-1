package com.example.lab3app.repository

import DepartmentPost
import EmployeeResponse
import android.os.Handler
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import com.example.lab3app.Application352
import com.example.lab3app.api.APPEND_DEPARTMENT
import com.example.lab3app.api.APPEND_EMPLOYEE
import com.example.lab3app.api.ApiInterface
import com.example.lab3app.api.DELETE_DEPARTMENT
import com.example.lab3app.api.DepartmentConnection
import com.example.lab3app.api.DepartmentResponse
import com.example.lab3app.api.EmployeePost
import com.example.lab3app.api.EnterprisePost
import com.example.lab3app.api.EnterpriseResponse
import com.example.lab3app.api.PostResult
import com.example.lab3app.api.UPDATE_DEPARTMENT
import com.example.lab3app.api.UPDATE_EMPLOYEE
import com.example.lab3app.data.Department
import com.example.lab3app.data.Employee
import com.example.lab3app.data.Enterprise
import com.example.lab3app.database.DepartmentDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val TAG = "com.example.lab3app.TAG"

class DepartmentRepository private constructor() {
    companion object {
        private var INSTANCE: DepartmentRepository? = null

        fun getInstance(): DepartmentRepository {
            if (INSTANCE == null) {
                INSTANCE = DepartmentRepository()
            }
            return INSTANCE
                ?: throw IllegalStateException("DepartmentRepository: Репозиторий не инициализирован")
        }
    }

    private val departmentDB by lazy {
        DBRepository(
            DepartmentDB.getDatabase(Application352.context).departmentDao()
        )
    }
    private val myCoroutineScope = CoroutineScope(Dispatchers.Main)

    var department: MutableLiveData<Department> = MutableLiveData()
    val departmentList: LiveData<List<Department>> = departmentDB.getDepartments().asLiveData()
    private var apiInterface = DepartmentConnection.getClient().create(ApiInterface::class.java)

    // Enterprise LiveData
    val enterpriseList: LiveData<List<Enterprise>> = departmentDB.getEnterprises().asLiveData()

    // Employee LiveData

    fun fetchDepartment() {
        apiInterface.getDepartments().enqueue(object : Callback<DepartmentResponse> {
            override fun onFailure(p0: Call<DepartmentResponse>, p1: Throwable) {
                Log.d(TAG, "Ошибка получения отделов", p1)
            }

            override fun onResponse(call: Call<DepartmentResponse>, response: Response<DepartmentResponse>) {
                if (response.code() == 200) {
                    val categories = response.body()
                    val items = categories?.items ?: emptyList()
                    Log.d(TAG, "Получен список отделов $items")
                    myCoroutineScope.launch {
                        departmentDB.deleteAllDepartments()
                        for (f in items) {
                            departmentDB.insert(f)
                        }
                    }
                }
            }
        })
    }

    fun fetchEnterprises() {
        apiInterface.getEnterprises().enqueue(object : Callback<EnterpriseResponse> {
            override fun onFailure(p0: Call<EnterpriseResponse>, p1: Throwable) {
                Log.d(TAG, "Ошибка получения предприятий", p1)
            }

            override fun onResponse(call: Call<EnterpriseResponse>, response: Response<EnterpriseResponse>) {
                if (response.code() == 200) {
                    val enterprises = response.body()
                    val items = enterprises?.items ?: emptyList()
                    Log.d(TAG, "Получен список предприятий $items")
                    myCoroutineScope.launch {
                        departmentDB.deleteAllEnterprises()
                        for (f in items) {
                            departmentDB.insertEnterprise(f)
                        }
                        Handler().postDelayed({
                            fetchDepartment()
                            fetchEmployeees()
                        }, 100)
                    }
                }
            }
        })
    }
    fun addNewEnterprise(enterprise: Enterprise) {
        apiInterface.postEnterprise(EnterprisePost(1, enterprise)) // Assuming action 1 is for adding a enterprise
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(call: Call<PostResult>, t: Throwable) {
                    Log.d(TAG, "Ошибка добавления предприятия", t)
                }

                override fun onResponse(call: Call<PostResult>, response: Response<PostResult>) {
                    Log.d(TAG, "Предприятие ${enterprise.name} было успешно добавлено")
                    fetchEnterprises()
                }
            })
    }
    fun updateEnterprise(enterprise: Enterprise) {
        apiInterface.updateEnterprise(EnterprisePost(32, enterprise))
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(call: Call<PostResult>, t: Throwable) {
                    Log.d(TAG, "Ошибка обновления предприятия", t)
                }

                override fun onResponse(call: Call<PostResult>, response: Response<PostResult>) {
                    if (response.code() == 200) {
                        Log.d(TAG, "Информация о предприятиии была успешно обновлена")
                        fetchEnterprises()
                    }
                }
            })
    }

    fun deleteEnterprise(enterprise: Enterprise) {
        apiInterface.deleteEnterprise(enterprise.id.toString())
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(call: Call<PostResult>, t: Throwable) {
                    Log.d(TAG, "Ошибка удаления предприятия", t)
                }

                override fun onResponse(call: Call<PostResult>, response: Response<PostResult>) {
                    if (response.code() == 200) {
                        Log.d(TAG, "Предприятие ${enterprise.name} было успешно удалено")
                        fetchEnterprises()
                    }
                }
            })
    }
    private fun newDepartment(departmentPost: DepartmentPost) {
        apiInterface.postDepartment(departmentPost)
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(p0: Call<PostResult>, p1: Throwable) {
                    Log.d(TAG, "Ошибка обновления отделов", p1)
                }

                override fun onResponse(p0: Call<PostResult>, p1: Response<PostResult>) {
                    if (p1.code() == 200) {
                        Log.d(TAG, "Отдел ${departmentPost.department.name} добавлен",)
                        fetchDepartment()
                    }

                }
            })
    }

    private fun updateDepartment(departmentPost: DepartmentPost) {
        apiInterface.updateDepartment(departmentId = departmentPost.department.id.toString(), departmentPost = departmentPost)
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(p0: Call<PostResult>, p1: Throwable) {
                    Log.d(TAG, "Ошибка обновления отделов", p1)
                }

                override fun onResponse(p0: Call<PostResult>, p1: Response<PostResult>) {
                    Log.d(TAG, "Информация об отделе была успешно обновлена")
                    if (p1.code() == 200) fetchDepartment()
                }
            })
    }

    private fun deleteDepartment(departmentPost: DepartmentPost) {
        apiInterface.deleteDepartment(departmentPost.department.id.toString())
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(p0: Call<PostResult>, p1: Throwable) {
                    Log.d(TAG, "Ошибка удаления категории", p1)
                }

                override fun onResponse(p0: Call<PostResult>, p1: Response<PostResult>) {
                    Log.d(TAG, "Отдел ${departmentPost.department.name} был успешно удален")
                    if (p1.code() == 200) fetchDepartment()
                }
            })
    }

    fun newDepartment(department: Department) {
        newDepartment(DepartmentPost(APPEND_DEPARTMENT, department))
    }

    fun deleteDepartment(department: Department) {
        deleteDepartment(DepartmentPost(DELETE_DEPARTMENT, department))
    }

    fun updatedepartment(department: Department) {
        updateDepartment(DepartmentPost(UPDATE_DEPARTMENT, department))
    }

    fun loadData() {
        fetchEnterprises()
        // Fetch enterprises
    }

    fun onDestroy() {
        myCoroutineScope.cancel()
    }

    fun setCurrentDepartment(_department: Department) {
        department.postValue(_department)
    }

    fun setCurrentDepartment(position: Int) {
        if (departmentList.value == null || position < 0 || (departmentList.value?.size!! <= position))
            return
        setCurrentDepartment(departmentList.value!![position])
    }

    fun getDepartmentPosition(department: Department): Int =
        departmentList.value?.indexOfFirst {
            it.id == department.id
        } ?: 1

    fun getDepartmentPosition() = getDepartmentPosition(department.value ?: Department())


    var employeeList: LiveData<List<Employee>> = departmentDB.getEmployees().asLiveData()
    var employee: MutableLiveData<Employee> = MutableLiveData()

    fun fetchEmployeees() {
        apiInterface.getEmployees().enqueue(object : Callback<EmployeeResponse> {
            override fun onFailure(p0: Call<EmployeeResponse>, p1: Throwable) {
                Log.d(TAG, "Ошибка получения сотрудников", p1)
            }

            override fun onResponse(call: Call<EmployeeResponse>, response: Response<EmployeeResponse>) {
                if (response.code() == 200) {
                    val employeees = response.body()
                    val items = employeees?.items ?: emptyList()
                    Log.d(TAG, "Получен список сотрудников $items")
                    myCoroutineScope.launch {
                        departmentDB.deleteAllEmployees()
                        for (f in items) {
                            departmentDB.insertEmployee(f)
                        }
                    }
                }
            }
        })
    }

    private fun newEmployee(employeePost: EmployeePost) {
        apiInterface.postEmployee(employeePost)
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(p0: Call<PostResult>, p1: Throwable) {
                    Log.d(TAG, "Ошибка добавления сотрудников", p1)
                }

                override fun onResponse(p0: Call<PostResult>, p1: Response<PostResult>) {
                    Log.d(TAG, "Сотрудник ${employeePost.employee.fio} был успешно нанят")
                    if (p1.code() == 200) fetchEmployeees()
                }
            })
    }

    private fun updateEmployee(employeePost: EmployeePost) {
        apiInterface.updateEmployee(employeePost.employee.id.toString(), employeePost)
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(p0: Call<PostResult>, p1: Throwable) {
                    Log.d(TAG, "Ошибка обновления сотрудника", p1)
                }

                override fun onResponse(p0: Call<PostResult>, p1: Response<PostResult>) {
                    Log.d(TAG, "Сотрудник ${employeePost.employee.fio} был успешно обновлен")
                    if (p1.code() == 200) fetchEmployeees()
                }
            })
    }

    private fun deleteEmployee(employeeId: String) {
        apiInterface.deleteEmployee(employeeId)
            .enqueue(object : Callback<PostResult> {
                override fun onFailure(p0: Call<PostResult>, p1: Throwable) {
                    Log.d(TAG, "Ошибка удаления сотрудника", p1)
                }

                override fun onResponse(p0: Call<PostResult>, p1: Response<PostResult>) {
                    Log.d(TAG, "Сотрудник был успешно удален")
                    if (p1.code() == 200) fetchEmployeees()
                }
            })
    }

    fun newEmployee(employee: Employee) {
        newEmployee(EmployeePost(APPEND_EMPLOYEE, employee))
    }

    fun updateEmployee(employee: Employee) {
        updateEmployee(EmployeePost(UPDATE_EMPLOYEE, employee))
    }

    fun deleteEmployee(employee: Employee) {
        deleteEmployee(employee.id.toString())
    }

    fun setCurrentEmployee(_employee: Employee) {
        employee.postValue(_employee)
    }

    fun setCurrentEmployee(position: Int) {
        if (employeeList.value == null || position < 0 || (employeeList.value?.size!! <= position))
            return
        setCurrentEmployee(employeeList.value!![position])
    }

    fun getEmployeePosition(employee: Employee): Int = employeeList.value?.indexOfFirst {
        it.id == employee.id
    } ?: 1

    fun getEmployeePosition() = getEmployeePosition(employee.value ?: Employee())

}