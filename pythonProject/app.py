from flask import Flask, request, jsonify
from uuid import uuid4
import sqlite3

app = Flask(__name__)

# Создание соединения с базой данных SQLite
db = sqlite3.connect('categories.db', check_same_thread=False)
cursor = db.cursor()

# Create the enterprises table if it doesn't exist
cursor.execute('''
       CREATE TABLE IF NOT EXISTS enterprises (
           id TEXT PRIMARY KEY,
           name TEXT NOT NULL
       )
   ''')
db.commit()

# Create the departments table if it doesn't exist
cursor.execute('''
       CREATE TABLE IF NOT EXISTS departments (
           id TEXT PRIMARY KEY,
           name TEXT NOT NULL,
           enterpriseId TEXT NOT NULL
       )
   ''')
db.commit()

# Create the employees table if it doesn't exist
cursor.execute('''
       CREATE TABLE IF NOT EXISTS employees (
           id TEXT PRIMARY KEY,
           fio TEXT NOT NULL,
           yearOfBirth INTEGER NOT NULL,
           position TEXT NOT NULL,
           department_id TEXT NOT NULL,
           phoneNumber TEXT,
           email TEXT,
           address TEXT,
           salary REAL,
           notes TEXT
       )
   ''')
db.commit()

# Генерация уникального ID
def generate_uuid():
    return str(uuid4())

# ... (your existing Flask code) ...

@app.route('/enterprise', methods=['POST'])
def create_enterprise():
    data = request.get_json()
    action = data.get('action')

    if action == 1:  # Assuming action 31 is for creating an enterprise
        enterprise_name = data.get('enterprises').get('name')
        if enterprise_name:
            enterprise_id = generate_uuid()
            cursor.execute(
                'INSERT INTO enterprises VALUES (?, ?)', (enterprise_id, enterprise_name)
            )
            db.commit()
            return jsonify({"result": "Enterprise created successfully"}), 201
        else:
            return jsonify({"result": "Enterprise name is missing"}), 400

    return jsonify({"result": "Invalid action"}), 400

@app.route('/', methods=['GET'])
def get_departments():
    code = request.args.get('code')
    if code is not None:
        code = int(code)
        if code == 10:  # Code for getting the list of departments
            cursor = db.cursor()
            cursor.execute('SELECT * FROM departments')
            departments = cursor.fetchall()
            cursor.close()
            return jsonify({'items': [{'id': row[0], 'name': row[1], 'enterpriseId': row[2]} for row in departments]})
        else:
            return jsonify({"result": "Invalid code"})
    else:
        return jsonify({"result": "Code not provided"})

@app.route('/enterprises', methods=['GET'])
def get_enterprises():
    cursor = db.cursor()
    cursor.execute('SELECT * FROM enterprises')
    enterprises = cursor.fetchall()
    cursor.close()
    return jsonify({'items': [{'id': row[0], 'name': row[1]} for row in enterprises]})

@app.route('/enterprises/<enterprise_id>/departments', methods=['GET'])
def get_departments_by_enterprise(enterprise_id):
    cursor = db.cursor()
    cursor.execute('SELECT * FROM departments WHERE enterpriseId = ?', (enterprise_id,))
    departments = cursor.fetchall()
    cursor.close()
    return jsonify({'items': [{'id': row[0], 'name': row[1], 'enterpriseId': row[2]} for row in departments]})

@app.route('/department', methods=['POST'])
def add_department():
    data = request.get_json()
    action = data.get('action')
    if action == 11:  # Code for adding a department
        new_department = data.get('departments')
        if "name" in new_department and "enterpriseId" in new_department:
            department_id = generate_uuid()
            cursor = db.cursor()
            cursor.execute('INSERT INTO departments VALUES (?, ?, ?)', (department_id, new_department["name"], new_department["enterpriseId"]))
            db.commit()
            cursor.close()
            return jsonify({"result": "Department added successfully"})
        else:
            return jsonify({"result": "Department name or enterpriseId missing in request"})
    else:
        return jsonify({"result": "Invalid code"})

@app.route('/enterprise', methods=['PUT'])
def update_enterprise():
    data = request.get_json()
    action = data.get('action')
    enterprise_id = data.get('enterprises').get('id')
    enterprise_name = data.get('enterprises').get('name')

    if action == 32:
        if enterprise_id and enterprise_name:
            cursor.execute(
                'UPDATE enterprises SET name = ? WHERE id = ?', (enterprise_name, enterprise_id)
            )
            db.commit()
            return jsonify({"result": "Enterprise updated successfully"}), 200
        else:
            return jsonify({"result": "Enterprise ID or name is missing"}), 400
    return jsonify({"result": "Invalid action"}), 400

# Endpoint for deleting an enterprise
@app.route('/enterprise/<enterprise_id>', methods=['DELETE'])
def delete_enterprise(enterprise_id):
    if enterprise_id:
        cursor.execute('DELETE FROM enterprises WHERE id = ?', (enterprise_id,))
        db.commit()
        return jsonify({"result": "Enterprise deleted successfully"}), 200
    else:
        return jsonify({"result": "Enterprise ID is missing"}), 400

# Обработчик для обновления отдела
@app.route('/department/<department_id>', methods=['PUT'])
def update_department(department_id):
    data = request.get_json()['departments']
    if 'name' in data:
        cursor.execute('UPDATE departments SET name=? WHERE id=?', (data['name'], department_id))
        db.commit()
        return jsonify({"result": "Department updated successfully"})
    else:
        return jsonify({"result": "Invalid data"}), 400

# Обработчик для удаления отдела
@app.route('/department/<department_id>', methods=['DELETE'])
def delete_department(department_id):
    cursor.execute('DELETE FROM departments WHERE id=?', (department_id,))
    db.commit()
    return jsonify({"result": "Department deleted successfully"})

# Обработчик для получения списка сотрудников
@app.route('/employees', methods=['GET'])
def get_employees():
    cursor.execute('SELECT * FROM employees')
    employees = cursor.fetchall()
    return jsonify({'items': [
        {'id': row[0], 'fio': row[1], 'yearOfBirth': row[2], 'position': row[3], 'department_id': row[4],
         'phoneNumber': row[5], 'email': row[6], 'address': row[7], 'salary': row[8], 'notes': row[9]}
         for row in employees]})

# Обработчик для добавления нового сотрудника
@app.route('/employee', methods=['POST'])
def add_employee():
    data = request.get_json()
    action = data.get('action')
    if action == 21:  # Code for adding an employee
        new_employee = data.get('employees')
        if "fio" in new_employee and "yearOfBirth" in new_employee and "position" in new_employee and "department_id" in new_employee:
            employee_id = generate_uuid()
            cursor = db.cursor()
            cursor.execute('INSERT INTO employees VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', (
                employee_id, new_employee["fio"], new_employee["yearOfBirth"], new_employee["position"], new_employee["department_id"],
                new_employee.get("phoneNumber"), new_employee.get("email"), new_employee.get("address"),
                new_employee.get("salary"), new_employee.get("notes")
            ))
            db.commit()
            cursor.close()
            return jsonify({"result": "Employee added successfully"})
        else:
            return jsonify({"result": "Employee data is missing in request"}), 400
    return jsonify({"result": "Invalid code"})

# Обработчик для обновления сотрудника
@app.route('/employee/<employee_id>', methods=['PUT'])
def update_employee(employee_id):
    data = request.get_json()['employees']
    if 'fio' in data and 'yearOfBirth' in data and 'position' in data and 'department_id' in data:
        cursor.execute('''
            UPDATE employees SET 
                fio=?, yearOfBirth=?, position=?, department_id=?, 
                phoneNumber=?, email=?, address=?, salary=?, notes=? 
            WHERE id=?
        ''', (
            data['fio'], data['yearOfBirth'], data['position'], data['department_id'],
            data.get('phoneNumber'), data.get('email'), data.get('address'), data.get('salary'),
            data.get('notes'), employee_id
        ))
        db.commit()
        return jsonify({"result": "Employee updated successfully"})
    else:
        return jsonify({"result": "Invalid data"}), 400

# Обработчик для удаления сотрудника
@app.route('/employee/<employee_id>', methods=['DELETE'])
def delete_employee(employee_id):
    cursor.execute('DELETE FROM employees WHERE id=?', (employee_id,))
    db.commit()
    return jsonify({"result": "Employee deleted successfully"})

if __name__ == '__main__':
    app.run(debug=True)