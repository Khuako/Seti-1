import os

class Config:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///app.db'  # Используем SQLite для простоты
    SQLALCHEMY_TRACK_MODIFICATIONS = False
