from datetime import datetime
from uuid import uuid4
from database import db

class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(db.String(36), primary_key=True, default=lambda: str(uuid4()))
    name = db.Column(db.String(255), nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }

class Dish(db.Model):
    __tablename__ = 'dishes'
    id = db.Column(db.String(36), primary_key=True, default=lambda: str(uuid4()))
    name = db.Column(db.String(255), nullable=False)
    weight = db.Column(db.Float, nullable=False)
    category_id = db.Column(db.String(36), db.ForeignKey('categories.id', ondelete='CASCADE'))
    price = db.Column(db.Float, nullable=False)
    available = db.Column(db.Boolean, default=False)
    description = db.Column(db.Text, nullable=True)
    ingredients = db.Column(db.Text, nullable=True)
    carbohydrates = db.Column(db.Float, nullable=True)
    calories = db.Column(db.Integer, nullable=True)
    short_description = db.Column(db.String(255), nullable=True)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'weight': self.weight,
            'category_id': self.category_id,
            'price': self.price,
            'available': self.available,
            'description': self.description,
            'ingredients': self.ingredients,
            'carbohydrates': self.carbohydrates,
            'calories': self.calories,
            'short_description': self.short_description
        }

    def __init__(self, name, weight, category_id, price, available, description, ingredients, carbohydrates, calories):
        self.name = name
        self.weight = weight
        self.category_id = category_id
        self.price = price
        self.available = available
        self.description = description
        self.ingredients = ingredients
        self.carbohydrates = carbohydrates
        self.calories = calories
        self.short_description = f"{name} ({weight})"
